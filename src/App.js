import React, { useState } from 'react';
import './App.css';
import { Card } from 'antd';
import 'antd/dist/antd.css';

function App() {
  const [user, setUser] = useState(undefined);
  const [active, setActive] = useState(false);
  const [name, setName] = useState(undefined);
  const [local, setLocal] = useState(undefined);
  const [repo, setRepo] = useState(undefined);
  const [imgURL, setImg] = useState(undefined);
  const handleToggle = () => {
    fetch("https://api.github.com/users/" + user)
      .then(res => res.json())
      .then(body => {
        setName(body.name);
        setLocal(body.location);
        setRepo(body.public_repos);
        setImg(body.avatar_url);
        setActive(!active);
      }
      );

  }
  const content = (input) => {
    setUser(input.target.value);
    setActive(false);
  }
  return (
    <div className="App">

      <h1 className="title">Github Cards - Search Dev's</h1>

      <input onChange={(e) => content(e)} className="input" /><button onClick={handleToggle}>Search</button>


      {imgURL && active && <a href={"https://www.github.com/" + user}>< Card
        hoverable
        style={{ width: 360, margin: "auto" }}
        cover={<img alt={user} src={imgURL} />}
      >
        <p>Nome: {name}</p>
        <p>Localização:{local}</p>
        <p>Repositórios públicos:{repo}</p>
      </Card>
      </a>
      }
    </div >
  );
}

export default App;
